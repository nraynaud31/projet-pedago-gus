Le site est [disponible sur surge](https://pwa-urgence.surge.sh/).  

## Marqueurs
La génération des marqueurs est gérée grâce au fichier icones.js.   
Les infos relatives aux marqueurs sont dans markers.json.   
Pour trouver les coordonnées GPS à partir d'une adresse postale : [Coordonnées GPS](https://www.coordonnees-gps.fr/).

## Textes
Afin de prendre en charge l'internationalisation, on s'appuie sur le fichier translate.js.   
Tous les textes affichés dans l'application sont dans les fichiers du dossier "messages". On trouve un fichier par langue disponible. La clé de chaque mot est l'id de l'élément HTML contenant cet élement.

## Mise à jour
Les infos sont à mettre à jour directement sur [un Airtable](https://airtable.com/tblvEravbCehbvBQx/viw1Cf2wdTCqfcv0r?blocks=hide) via un formulaire en ligne fourni aux services concernés. 
La génération du JSON (markers_airtable.json) se fait via un script. Pour lancer celui-ci :

```
node generate_json.js
```

Ensuite, vérifier que rien n'est cassé et pousser sur Surge.  
Attention, il y a besoin d'un identifiant et d'une clé pour accéder à l'API Airtable. Il suffit de demander pour les avoir. ;-)

## Contribuer
Le code est accessible [sur gitlab](https://gitlab.com/ldevernay/projet-azimut).   
Toutes les contributions sont les bienvenues.   
Il y a de quoi s'occuper dans les Issues et Requirements. 
**Chaque modif doit faire l'objet d'une merge-request.**
